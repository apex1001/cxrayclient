<?php


//Lookup a address and return latitude + longitude
//There is a resolving max at googleapis for one IP (12.000 a month / 2.500 a day)
//Orginal based on http://www.andrew-kirkpatrick.com/2011/10/google-geocoding-api-with-php/
//More info about geocoding https://developers.google.com/maps/documentation/geocoding/?hl=nl&csw=1
function lookup($address){
   $address = str_replace (" ", "+", urlencode($address));
   $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address."&sensor=false";
 
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $details_url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   $response = json_decode(curl_exec($ch), true);
 
   // If Status Code is not OK return null
   if ($response['status'] != 'OK') {
    return null;
   }
 
   //print_r($response);  
   $geometry = $response['results'][0]['geometry'];
    $longitude = $geometry['location']['lat'];
    $latitude = $geometry['location']['lng'];
 
    $array = array(
        'latitude' => $geometry['location']['lng'],
        'longitude' => $geometry['location']['lat'],
    );
 
    return $array;
}
 
$address = 'de taats 11 buitenpost';
 
$array = lookup($address);
print_r($array);

?>




<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKDioRbD5kouSyoNiVPfU0e7RGi_LAP8o&sensor=true">
    </script>
    <script type="text/javascript">
      function initialize(a) {
		window.alert(a);
		var latitude = <?php Print($array["latitude"]); ?>;
		var longitude = <?php Print($array["longitude"]); ?>;
		
        var mapOptions = {
          center: new google.maps.LatLng(longitude, latitude),
          zoom: 18
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
			
		// Creating a marker and positioning it on the map    
		var marker = new google.maps.Marker({    
		position: new google.maps.LatLng(longitude, latitude),    
		  map: map,
		title: 'Het bedrijf...',
		icon: 'http://google-maps-icons.googlecode.com/files/factory.png'  
		}); 
		marker.info = new google.maps.InfoWindow({
		  content: '<b>Bedrijf :</b> iets.....'
		});
		marker.info.open(map, marker); //Show company marker info 
		google.maps.event.addListener(marker, 'click', function() {
		  marker.info.open(map, marker); //Show company marker info 	
		});
		
      }
      //google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body onload="initialize('hallo')">
    <div id="map-canvas"/>
  </body>
</html>

