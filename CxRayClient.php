<?php
/*
 * Main CX-Ray. 
 * 
 * @author V.Vogelesang
 * 
 */

	include_once 'CxRayComponents.php';
	
	// Print the complete index search page
	printHtmlHeader("js/CxRayClient.js","");
	printHeaderPadding();
	printHeader();
	printSearchBox();
	printHtmlFooter();
?>