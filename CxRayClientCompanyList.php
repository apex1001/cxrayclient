<?php
/*
 * Company list page for CX-Ray.
*
* @author V.Vogelesang
*
*/
	include_once 'CxRayComponents.php';
	
	// Print the company list page & results
	printHtmlHeader("js/CxRayClientCompanyList.js","createCompanyListRequest()");
	printSearchUrl($_POST['searchUrl']);
	printHeader();
	printSearchResults("Select company & options","CxRayClientCompanyResult.php");
	printHtmlFooter();
	

?>
