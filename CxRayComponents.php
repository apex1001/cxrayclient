<?php
/*
 * Contains all HTML components for the CX-Ray client
 * 
 * @author V.Vogelesang
 */

	/**
	 * Print the Html header
	 */
	function printHtmlHeader($javascript, $function) {
		echo '
			<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
			<link rel="stylesheet" href="css/default.css">	
			<title>CX-Ray client</title>
			</head>
			<script src="js/jquery-1.11.0.min.js"></script>
			<script src="js/raphael.js"></script>
			<script src="js/g.raphael.js"></script>
			<script src="js/g.bar.js"></script>
	    	<script src="js/graphing.js"></script>
		
			<script src="' . $javascript . '"></script>
			<body onload="' . $function . '">
		';
	}

	/**
	 * Print the padding between top and header/log
	 */
	function printHeaderPadding () {
		echo '
			<div id="headerPadding"></div>				
			';
	}

	/**
	 * Print the CX-Ray header/logo
	 */
	function printHeader() {
		echo '
		<div id="header">
			<div id="headerLeft">
				<img src="img/cxraydualline.jpg"></img>
			</div>
			<div id="headerRight">
				<img src="img/cxraydualline.jpg"></img>
			</div>
			<div id="headerCenter">
				<img src="img/cxraylogo.jpg"></img>
			</div>
		</div>
		';
	}

	/**
	 * Print the searchBox
	 */
	function printSearchBox() {
		echo '		
			<div id="searchBox">
				<form id="searchCompany" method="post">
					Search company:
					<input class="rounded" id="search" type="text" size="34">
					<input class="rounded" type="button" value="Search!" onclick="createSearchRequest()"/>
					<br/><br/>';
					printSearchItemBoxes();
		echo '	</form>
			</div>
		';
	}

	/**
	 * Print the search result box
	 * 
	 * @param text in search box header
	 * @param action on submit
	 */
	function printSearchResults($headerText, $action) {
		echo '
			<div id="searchResult">
				<div id="resultTop">
					<div id="resultTopText">
						<table>
							<tr class="rounded">
								<td class="alignLeft">' . $headerText . '</td>
								<td class="alignRight">
										<form method="post" action="CxRayClient.php">
											<input class="rounded" type="submit" value="New search"/>	
										</form>
     							</td>
							</tr>
						</table>
					</div>
				</div>			
				<div id="resultMiddle">
					<div id="resultContainer">
					</div>
				</div>
				<div id="resultBottom">
					<div id="resultBottomText">
						<form id="itemForm" method="post" action="' . $action . '">'; 
							printSearchItemBoxes();
		echo '			</form>
					</div>
				</div>
			</div>
		';
	}

	/**
	 * Print the Html footer
	 */
	function printHtmlFooter() {
		echo '
			</body>
			</html>
		';
	}
	
	/**
	 * Check if a box is checked and
	 * return 'checked' for html purposes
	 * 
	 * @param checkbox name
	 * @return string
	 */
	function boxChecked($name) {
		if (isset($_POST[$name])) {
			return 'checked';
		}
		else return '';
	}

	/**
	 * Print the search url from the main page into javascript
	 * @param $url
	 */
	function printSearchUrl($url) {
		echo '<script>searchUrl = "' . $url . '";</script>';
	}
	
	/**
	 * Print the company url from the companylist page into javascript
	 * @param $url
	 */
	function printCompanyUrl($url) {
		echo '<script>companyUrl = "' . $url . '";</script>';
	}
	
	/**
	 * Print searchitem boxes
	 */
	function printSearchItemBoxes() {
		echo '
			Search for
			<input type="checkbox" id="news" name="news" value="1" ' . boxChecked('news') . '>News
			<input type="checkbox" id="location" name="location" value="1" ' . boxChecked('location') . '>Location			
			<input type="checkbox" id="finance" name="finance" value="1" ' . boxChecked('finance') . '>Finance
			<input type="checkbox" id="jobs" name="jobs" value="1" ' . boxChecked('jobs') . '>Jobs
			<input type="checkbox" id="linkedin" name="linkedin" value="1" ' . boxChecked('linkedin') . '>LinkedIn			
			<input type="checkbox" id="rating" name="rating" value="1" ' . boxChecked('rating') . '>Rating
			';
	}
	
	/**
	 * Include the google geocoding script
	 */
	function printGoogleGeoCodingScript() {
		echo '   
				<script type="text/javascript"
      				src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKDioRbD5kouSyoNiVPfU0e7RGi_LAP8o&sensor=true">
    			</script>
				<script src="js/GoogleGeoCoding.js"></script>
				';		
	}
	
?>
