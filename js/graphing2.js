/**
 * Draws all the graphs. Some code borrowed from
 * http://g.raphaeljs.com/barchart2.html
 * http://g.raphaeljs.com/piechart2.html
 * 
 * @author Vincent Vogelesang
 */
window.onload = function () {
	
	drawBarGraph();
	drawPieGraph();
	
	function drawBarGraph() {
		// Init variables
		var chartX = 5;
	  	var chartY = 20;
	  	var chartXsize = 450;
	  	var chartYsize = 270;
	  	var periods = allPeriodsList[0].length -1;
	  	var labelArray = new Array();
	  	for (i = 0; i < periods; i++) {
	  		labelArray[i]= "" + (periods - i);
	  	}
	  	labelArray[labelArray.length] = "Totaal";
	    
	  	// Draw canvas, insert hovers
		var r = Raphael("barholder"),
	    	fin = function () {
				this.flag = r.popup(this.bar.x, this.bar.y, this.bar.value || "0").insertBefore(this);
	      	},
	        fout = function () {
	      		this.flag.animate({opacity: 0}, 300, function () {this.remove();});
	      	},
	      	fin2 = function () {
	      		var y = [], res = [];
	      		for (var i = this.bars.length; i--;) {
	      			y.push(this.bars[i].y);
	      			res.push(this.bars[i].value || "0");
	      		}
	      		this.flag = r.popup(this.bars[0].x, Math.min.apply(Math, y), res.join(", ")).insertBefore(this);
	      	},
	      	fout2 = function () {
	      		this.flag.animate({opacity: 0}, 300, function () {this.remove();});
	      	},
	      	txtattr = { font: "12px sans-serif" }; 
	      	
	      	// Draw bargraph, labels
	      	r.text(chartX + chartXsize/2 , chartY + chartYsize + 20, "Periode overzicht").attr(txtattr);
	      	r.barchart(chartX,chartY, chartXsize, chartYsize, allPeriodsList).hover(fin, fout);
	      	Raphael.g.axis(chartX + (chartYsize/labelArray.length), chartY + chartYsize + 10, chartXsize- (chartYsize/labelArray.length*2), null, null, labelArray.length-1, 2, labelArray, " ", 0, r);
	      	
	      	// Draw axis'
	    	var xaxis = r.path(["M", chartX, chartYsize, "L", chartXsize +10, chartYsize ]);
	       	var yaxis = r.path(["M", chartX, chartY, "L", chartX, chartYsize ]);      	
	}
	
	function drawPieGraph() {
		
		// Remove own activities from allCategoryGradelist if in top 10 
		for (i = 0; i < activityList.length; i++) {
			for (j = 0; j < allCategoryGradeList.length; j++) {
				if (allCategoryGradeList[j].substring(2, allCategoryGradeList[j].length) == activityList[i]) {
					allCategoryGradeList.splice(j, 1);
					break;
				}
			}			
		}
		
		// Add own grades to top grades for category
		var gradeList = new Array();
		for (i = 0; i < allCategoryGradeList.length; i++) {
	  		gradeList[i] = parseInt(allCategoryGradeList[i].substring(0,1));
	  	}
		gradeList = gradeList.concat(ownGradeList);
		console.log(gradeList);
		
		// Populate legend array
		var legendArray = new Array();
		for (i = 0; i < allCategoryGradeList.length; i++) {
	  		legendArray[i] = "## " + allCategoryGradeList[i].substring(2, allCategoryGradeList[i].length);
	  	}
		var legendLength = legendArray.length;
		for (i = 0; i < activityList.length; i++) {
	  		legendArray[legendLength + i] = "## " + activityList[i];
	  	}
				
		// Draw canvas
		var r = Raphael("pieholder"),
        pie = r.piechart(140, 135, 120, gradeList, { legend: legendArray, legendpos: "east"});
		pie.hover(function () {
			this.sector.stop();
			this.sector.scale(1.1, 1.1, this.cx, this.cy);

			if (this.label) {
				this.label[0].stop();
				this.label[0].attr({ r: 7.5 });
				this.label[1].attr({ "font-weight": 800 });
			}
		}, function () {
				this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");

				if (this.label) {
					this.label[0].animate({ r: 5 }, 500, "bounce");
					this.label[1].attr({ "font-weight": 400 });
				}
			});
	}	
};

    