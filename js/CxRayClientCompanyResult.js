/*
 * Javascript source for CxRayClientCompanyResult.php.
 * Part of the CX-Ray client app
 * 
 * @author V. Vogelesang
 */

	/**
	 * Create a GET request for the
	 * given company id and items.
	 * 
	 */
	function createCompanyResultRequest() {
		// Get global var from PHP $_POST['searchUrl']		
		
		// code to circumvent xss restriction, remove when on cxray.localhost.nl
		var id = companyUrl.substring(companyUrl.lastIndexOf('/') + 1);	
		var url = "http://localhost/cxraylocalhostnl/company/" + id; 		
		
		//Unremark when on cxray.localhost.nl	
		//var url = companyUrl; 	
		window.setTimeout(function(){executeAjaxCall(url);},1200);
	}			
	
	/**
	 * Get the data from CX Ray
	 */
	function executeAjaxCall(url) {
		$.ajax({ 
			type: "GET",
	        url: url,
	        contentType: "application/xml",
	        dataType: "xml",
	        cache: false,
	        async: false,
	        error: function() { window.alert("No data found."); },
	        success: function(data, status, xhr) {                
	            // Fill result table with xml data
	            fillResultContainer(data);                
	        } 
		});			
	}
	
	/**
	 * Fill the result container with all items
	 * 
	 * @param xml
	 */
	function fillResultContainer(xml) {		
		fillCompanyDetails(xml);
		fillNewsItems(xml);
		fillJobDetails(xml);
		fillLinkedInDetails(xml);
		fillRating(xml);
		fillStockHistory(xml);
		fillLocation(xml);		
	}
	
	/**
	 * Fill the company details div
	 * @param xml
	 */
	function fillCompanyDetails(xml) {
		// Add resultDiv to resultContainer
		var divId = "companyDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "Company Details"); 
		
		// Create companyDetailsTable
		var companyDetailsTable = createDetailsTable(divId, 'companyDetailsTable'); 	

		// Add Company details to table		
		var companyDetailArray = {
				"name": "Company name", 
				"address": "Address",
				"postcode": "Zipcode",
				"city": "City",
				"kvkNumber": "KvKNumber",
				"establishmentNumber": "Establishment number"
		};
		
		// Add cell data
		for (key in companyDetailArray)	{
			var row = companyDetailsTable.insertRow(-1);
			var cell = row.insertCell(-1);
			cell.innerHTML = companyDetailArray[key];
			var cell = row.insertCell(-1);
			cell.innerHTML = xml.getElementsByTagName(key)[0].firstChild.nodeValue;
		}		
	}
	
	/**
	 * Fill the news items div
	 * 
	 * @param xml
	 */
	function fillNewsItems(xml) {
		// Add resultDiv to resultContainer
		var divId = "newsDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "News"); 
		
		// Create companyDetailsTable
		var newsDetailsTable = createDetailsTable(divId, 'newsDetailsTable'); 			
		
		// Add Company details to table		
		var newsItemArray = xml.getElementsByTagName('newsItem');
		
		// Add cell data
		for (var i = 0; i < newsItemArray.length; i++) {
			childNodes = newsItemArray[i].childNodes;
			var row = newsDetailsTable.insertRow(-1);
			var cell = row.insertCell(-1);
			cell.innerHTML = '<a href="' + 
						childNodes[0].firstChild.nodeValue + '">' + 
						childNodes[1].firstChild.nodeValue;
		}		
	}
	
	/**
	 * Fill the news items div
	 * 
	 * @param xml
	 */
	function fillJobDetails(xml) {
		// Add resultDiv to resultContainer
		var divId = "jobsDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "Jobs"); 
		
		// Create jobsDetailsTable
		var jobsDetailsTable = createDetailsTable(divId, 'jobsDetailsTable'); 			
		
		// Add Company details to table		
		var newsItemArray = xml.getElementsByTagName('jobOpening');
		
		// Add cell data
		for (var i = 0; i < newsItemArray.length; i++) {
			childNodes = newsItemArray[i].childNodes;
			var row = jobsDetailsTable.insertRow(-1);
			var cell = row.insertCell(-1);
			cell.innerHTML = '<a href="' + 
						childNodes[0].firstChild.nodeValue + '">' + 
						childNodes[1].firstChild.nodeValue;
		}		
	}
	
	/**
	 * Fill the company details div
	 * @param xml
	 */
	function fillLinkedInDetails(xml) {
		// Add resultDiv to resultContainer
		var divId = "linkedInDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "LinkedIn"); 
		
		// Create companyDetailsTable
		var linkedInDetailsTable = createDetailsTable(divId, 'linkedInDetailsTable'); 	

		// Add Company details to table		
		var linkedInDetailArray = {
				"type": "Type", 
				"companySize": "Company size",
				"website": "Website URL",
				"branch": "Branch",
				"founded": "Founded in"
		};
		
		// Add cell data
		for (key in linkedInDetailArray)	{
			var row = linkedInDetailsTable.insertRow(-1);
			var cell = row.insertCell(-1);
			cell.innerHTML = linkedInDetailArray[key];
			var cell = row.insertCell(-1);
			cell.innerHTML = xml.getElementsByTagName(key)[0].firstChild.nodeValue;
		}		
	}
	
	/**
	 * Fill the rating info and buttons
	 * @param xml
	 */
	function fillRating(xml) {
		// Add resultDiv to resultContainer
		var divId = "ratingDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "Rating"); 
		
		// Create companyDetailsTable
		var ratingDetailsTable = createDetailsTable(divId, 'ratingDetailsTable'); 
		
		// Show the rating
		var row = ratingDetailsTable.insertRow(-1);
		var cell = row.insertCell(-1);
		cell.innerHTML = 'Company average rating';
		var cell = row.insertCell(-1);
		var rating = xml.getElementsByTagName('averageRating')[0].firstChild.nodeValue;
		cell.innerHTML = Math.round(rating);

		// Create the rating buttons
		var row = ratingDetailsTable.insertRow(-1);
		var cell = row.insertCell(-1);
		var radioString = '';
		
		for (var i=1; i < 6; i++) {
			radioString += i + '<input type="radio" name="rating" value="' + i + '">';
		}
		cell.innerHTML = radioString;
		var cell = row.insertCell(-1);
		cell.innerHTML = '<input type="button" value="Rate" onclick="rateCompany()">';
		
	}
	
	/**
	 * Write the company rating to the CX Ray facade
	 * 
	 */
	function rateCompany() {
				
		// Get rating from radio selector
		var rating = document.querySelector('input[name="rating"]:checked').value;		
		
		// code to circumvent xss restriction, remove when on cxray.localhost.nl
		var id = companyUrl.substring(companyUrl.lastIndexOf('/') + 1);	
		var url = "http://localhost/cxraylocalhostnl/company/" + id; 		
		
		//Unremark when on cxray.localhost.nl	
		//var url = companyUrl; 	
		
		console.log(url);
		var patchUrl = url.substring(0,url.lastIndexOf('?'));
		console.log(patchUrl);
		var xmlData = '<rating><value>' + rating + '</value></rating>';
		
		// Update rating for company
		$.ajax({ 
			type: "PATCH",
	        url: patchUrl,
	        contentType: "application/xml",
	        dataType: "xml",
	        data: xmlData,
	        cache: false,
	        async: false,
	        error: function() { window.alert("Error! Try again"); },
	        success: function(data, status, xhr) { 
	        	window.alert("Thank you!"); 
	        	getCompanyResult(url);
	        	}
		});				
	}
	
	/**
	 * Show company on map
	 * 
	 * @param xml
	 */
	function fillLocation(xml) {
		// Add resultDiv to resultContainer
		var divId = "locationDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "Location"); 
		
		// Get all variables for google geocoding
		var title = xml.getElementsByTagName('name')[0].firstChild.nodeValue;
		var longitude = xml.getElementsByTagName('longitude')[0].firstChild.nodeValue;
		var latitude = xml.getElementsByTagName('latitude')[0].firstChild.nodeValue;		
		
		// Run google api
		googleInitialize(longitude, latitude, divId, title);		 
	}
	
	/**
	 * Fill the stock history div with a graph
	 * 
	 * @param xml
	 */
	function fillStockHistory(xml) {
		// Add resultDiv to resultContainer
		var divId = "financeDetails";
		createResultDiv(document.getElementById("resultContainer"), divId, "Finance"); 
		
		var stockArray = xml.getElementsByTagName('stockHistory');
		var simpleStockArray = new Array();
		// Add cell data
		for (var i = 0; i < stockArray.length; i++) {
			childNodes = stockArray[i].childNodes;
			var stockObject = {};
			var date = childNodes[0].firstChild.nodeValue;
			var price = childNodes[3].firstChild.nodeValue;
			stockObject['date'] = date;
			stockObject['price'] = price; 
			simpleStockArray.push(stockObject);
		}		
		createResultDiv(document.getElementById(divId), "barDiv", "");
		drawBarGraph(simpleStockArray.reverse(), document.getElementById("barDiv"));
	}
	
	/**
	 * Create a div for the item results
	 * 
	 * @param rootDiv
	 * @param id
	 */
	function createResultDiv(rootDiv, id, name) {
		// Create div legend		
		var legend = document.createElement('legend');
		var text = document.createTextNode(name);
		legend.appendChild(text);
		
		// Create div
		var resultDiv = document.createElement('div');
		resultDiv.className = 'resultDiv';
		resultDiv.id = id;
		resultDiv.title = 'test';
		resultDiv.appendChild(legend);
		rootDiv.appendChild(resultDiv);
	}
	
	/**
	 * Create a table with id in a given div
	 * @param divId
	 * @param tableId
	 */
	function createDetailsTable(divId, tableId) {
		
		var div = document.getElementById(divId);
		var table = document.createElement('table');
		table.id = tableId;
		div.appendChild(table);	
		return table;
	}
	
	/**
	 * Post itemform to next page, 
	 * with added companyUrl
	 * 
	 * @param url for company info
	 */
	function getCompanyResult(url) {
		
		// Create hidden field to hold search url
		var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "companyUrl");
        hiddenField.setAttribute("id", "companyUrl");
        hiddenField.setAttribute("value", url);
        document.getElementById("itemForm").appendChild(hiddenField);
        
        // Submit the data
        var searchForm = document.getElementById("itemForm");
        searchForm.action = "CxRayClientCompanyResult.php";
        searchForm.submit();
	}

	