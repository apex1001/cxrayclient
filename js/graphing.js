/**
 * Draws all the graphs. Some code borrowed from
 * http://g.raphaeljs.com/barchart2.html
 * 
 * @author Vincent Vogelesang
 */
	
	function drawBarGraph(stockArray, divId) {
		// Init variables
		var chartX = 5;
	  	var chartY = 20;
	  	var chartXsize = 450;
	  	var chartYsize = 200;
	  	var allPeriodsList = new Array();
	  	var labelArray = new Array();
		
	  	// Clear div border
	  	divId.className = '';
	  		  	
	  	// Add labels to horizontal bar
	  	for (var i = 0; i < stockArray.length; i++) {
	  		labelArray[i]= stockArray[i]['date'].substring(5,7);	  		
	  	}
	  	
	  
	  	// Add data
	  	for (var i = 0; i < stockArray.length; i++) {
	  		allPeriodsList[i]= stockArray[i]['price'];	  		
	  	}	  	
	    
	  	// Draw canvas, insert hovers
		var r = Raphael(divId),
	    	fin = function () {
				this.flag = r.popup(this.bar.x, this.bar.y, this.bar.value || "0").insertBefore(this);
	      	},
	        fout = function () {
	      		this.flag.animate({opacity: 0}, 300, function () {this.remove();});
	      	},
	      	fin2 = function () {
	      		var y = [], res = [];
	      		for (var i = this.bars.length; i--;) {
	      			y.push(this.bars[i].y);
	      			res.push(this.bars[i].value || "0");
	      		}
	      		this.flag = r.popup(this.bars[0].x, Math.min.apply(Math, y), res.join(", ")).insertBefore(this);
	      	},
	      	fout2 = function () {
	      		this.flag.animate({opacity: 0}, 300, function () {this.remove();});
	      	},
	      	txtattr = { font: "12px sans-serif" }; 
	      	
	      	// Draw bargraph, labels
	      	r.text(chartX + chartXsize/2 , chartY + chartYsize + 20, "Months").attr(txtattr);
	      	r.barchart(chartX,chartY, chartXsize, chartYsize, [allPeriodsList]).hover(fin, fout);
	      	Raphael.g.axis(chartX + (chartYsize/labelArray.length), chartY + chartYsize + 10, chartXsize- (chartYsize/labelArray.length*5/2), null, null, labelArray.length-1, 2, labelArray, " ", 0, r);
	      	
	      	// Draw axis'
	    	var xaxis = r.path(["M", chartX, chartYsize, "L", chartXsize +10, chartYsize ]);
	       	var yaxis = r.path(["M", chartX, chartY, "L", chartX, chartYsize ]);      	
	}
	


    