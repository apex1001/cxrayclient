/*
 * Javascript source for CxRayClientCompanyList.php.
 * Part of the CX-Ray client app
 * 
 * @author V. Vogelesang
 */

	/**
	 * Create a GET request for the
	 * given search id and items.
	 * 
	 */
	function createCompanyListRequest() {
			
		// Get global var from PHP $_POST['searchUrl']		
		
		// code to circumvent xss restriction, remove when on cxray.localhost.nl
		var id = searchUrl.substring(searchUrl.lastIndexOf('/') + 1);	
		var url = "http://localhost/cxraylocalhostnl/search/" + id; 
		
		//Unremark when on cxray.localhost.nl	
		//var url = searchUrl; 	
		window.setTimeout(function(){executeAjaxCall(url);},1200);
	}			
	
	/**
	 * Get the data from CX Ray
	 */
	function executeAjaxCall(url) {
		$.ajax({ 
			type: "GET",
            url: url,
            contentType: "application/xml",
            dataType: "xml",
            cache: false,
            async: false,
            error: function() { window.alert("No data found."); },
            success: function(data, status, xhr) {                
                // Fill result table with xml data
                fillTable(data);                
            } 
		});			
	}
	
	/**
	 * Adds xml data to resultTable
	 * @param xml
	 */
	function fillTable(xml) {
		
		// Create resultTable
		var resultContainer = document.getElementById('resultContainer');
		var resultTable = document.createElement('table');
		resultTable.id = 'resultTable';
		resultContainer.appendChild(resultTable);		
		
		// Get all result nodes and add contents to resultTable
		var resultList = xml.getElementsByTagName("result");		
		
		// Inser table header
		var headerNames = new Array("Name","Address","Zipcode","City","Establishment");
		var row = resultTable.insertRow(-1);
		
		for (var i = 0; i < headerNames.length; i++) {
			header = document.createElement('th');
			header.innerHTML = headerNames[i];
			row.appendChild(header);
		}
				
		// Insert table data
		for (var i = 0; i < resultList.length; i++) {
			childNodes = resultList[i].childNodes;		
			var row = resultTable.insertRow(-1);
			row.id = resultList[i].getElementsByTagName("url")[0].firstChild.nodeValue;
			row.onclick = function() { getCompanyResult(this.id); };
			for (var j = 1; j < childNodes.length; j++) {
				var cell = row.insertCell(-1);
				cell.innerHTML = childNodes[j].firstChild.nodeValue;				
			}			
		}
		
		/**
		 * Post itemform to next page, 
		 * with added companyUrl
		 * 
		 * @param url for company info
		 */
		function getCompanyResult(url) {
			
			// Create hidden field to hold search url
			var hiddenField = document.createElement("input");
	        hiddenField.setAttribute("type", "hidden");
	        hiddenField.setAttribute("name", "companyUrl");
	        hiddenField.setAttribute("id", "companyUrl");
	        hiddenField.setAttribute("value", url);
	        document.getElementById("itemForm").appendChild(hiddenField);
	        
	        // Submit the data
	        var searchForm = document.getElementById("itemForm");
	        searchForm.action = "CxRayClientCompanyResult.php";
	        searchForm.submit();
		}
	}
