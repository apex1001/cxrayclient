/*
 * Javascript source for CxRayClient.php.
 * Part of the CX-Ray client app
 * 
 * @author V. Vogelesang
 */

	/**
	 * Create a search request for the
	 * given company string and items.
	 * 
	 */
	function createSearchRequest() {
		var url = "http://localhost/cxraylocalhostnl/search";
		var xmlData = getFormData(document.getElementById("search").value);	
				
		$.ajax({ 
			type: "POST",
            url: url,
            data: xmlData,            
            contentType: "application/xml",
            cache: false,
            error: function() { window.alert("No data found."); },
            success: function(data, status, xhr) {
                // Go to next page and post response url with search id
                getCompanyList(xhr.getResponseHeader('Location'));
            } 
		});			
	}
	
	/**
	 * Post search form to next page, 
	 * with added search url
	 * 
	 */
	function getCompanyList(url) {
		
		// Create hidden field to hold search url
		var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "searchUrl");
        hiddenField.setAttribute("id", "searchUrl");
        hiddenField.setAttribute("value", url);
        document.getElementById("searchCompany").appendChild(hiddenField);
        
        // Submit the data
        var searchForm = document.getElementById("searchCompany");
        searchForm.action = "CxRayClientCompanyList.php";
        searchForm.submit();
	}
	
	/**
	 * Build the XML string for given company and options
	 * 
	 * @param companyName
	 * @returns xmlData
	 */
	function getFormData(companyName) {
		$items = "";
		if (document.getElementById("news").checked) $items += "<item>news</item>";
		if (document.getElementById("linkedin").checked) $items += "<item>linkedin</item>";
		if (document.getElementById("location").checked) $items += "<item>location</item>";
		if (document.getElementById("finance").checked) $items += "<item>finance</item>";
		if (document.getElementById("jobs").checked) $items += "<item>jobs</item>";
		//if (document.getElementById("rating").checked) $items += "<item>rating</item>";
			
		var xmlString = "<search>" +
							"<string>" + companyName + "</string>" +
							"<items>" +
								$items +
							"</items>" +
						"</search>";
		return xmlString;		
	}
	