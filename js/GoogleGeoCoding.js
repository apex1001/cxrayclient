/*
 * Google maps code for CX-Ray
 * 
 * @author Google
 * Orginally based on http://www.andrew-kirkpatrick.com/2011/10/google-geocoding-api-with-php/
 * More info about geocoding https://developers.google.com/maps/documentation/geocoding/?hl=nl&csw=1
 */	

	/** 
	 * Show the map in given div
	 */
	function googleInitialize(latitude, longitude, divId, title) {
		
		var mapOptions = {
	      center: new google.maps.LatLng(longitude, latitude),
	      zoom: 18
	    };
	    var map = new google.maps.Map(document.getElementById(divId),
	        mapOptions);
			
		// Creating a marker and positioning it on the map    
		var marker = new google.maps.Marker({    
		position: new google.maps.LatLng(longitude, latitude),    
		  map: map,
		title: title,
		icon: 'http://google-maps-icons.googlecode.com/files/factory.png'  
		}); 
		marker.info = new google.maps.InfoWindow({
		  content: title
		});
		marker.info.open(map, marker); //Show company marker info 
		google.maps.event.addListener(marker, 'click', function() {
		  marker.info.open(map, marker); //Show company marker info 	
			});			
	}
