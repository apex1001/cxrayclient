<?php
/*
 * Company result page for CX-Ray.
 *
 * @author V.Vogelesang
 */

	include_once 'CxRayComponents.php';
	
	// Print results page
	printHtmlHeader("js/CxRayClientCompanyResult.js","createCompanyResultRequest()");
	printCompanyUrl($_POST['companyUrl']);
	printGoogleGeoCodingScript();
	printHeader();
	printSearchResults("Search results","CxRayClientCompanyResult.php");
	printHtmlFooter();
?>